<?php  

 // $language  = get_query_var( 'en');
 $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>

<?php
/**
 * Template Name: Retailer search
 *
 * 
 */

get_header(); ?>


 		<!--form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
   			 <input class="text" class="form-control" placeholder="Search for..."value="<?php _e('Search for...');?>" type="text" value=" " name="s" id="s" /></div>
				<span class="input-group-btn">
				<button class="btn btn-default submit button" name="submit" value="<?php _e('Search');?>" type="submit"><i class="		glyphicon glyphicon-search"></i></button>
  			  </span>
   
		</form-->

<div   class="container content"> 
	<div class="main-content ">
		<div class="row content-text search-section">
			<div class="col-lg-4 search-col">
			<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
			<div class="input-group">
				<input type="text" class="form-control" name="s" placeholder="Search Retailers"/>
				<input type="hidden" name="post_type" value="retailer" /> <!-- // hidden 'products' value -->
				<span class="input-group-btn">
						<button class="btn btn-default" alt="Search" class="btn btn-default"  type="submit"><i class="glyphicon glyphicon-search"></i></button>
					

				<!--button type="submit" class="btn btn-default" alt="Search" class="btn btn-default" ><i class="glyphicon glyphicon-search"></i></button-->
				</span>
				</div>
              </form>
			</div>
		</div>
		<div class="row content-text ">


   


<?php
    if ( isset( $_REQUEST[ 'search' ] ) ) {
        // run search query
        query_posts( array(
            's' => $_REQUEST[ 'search' ],
            'post_type' => $_REQUEST[ 'retailer' ],
            'paged' => $paged
        ) );

        // loop
        if ( have_posts() ) : while ( have_posts() ) :
            // loop through results here
        endwhile; endif;

        // return to original query
        wp_reset_query();
    }
?>

 


<?php 

  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

  $query_args = array(
  		's' => $_GET['s'],
      'post_type' => 'retailer',
      'posts_per_page' => 9,
      'paged' => $paged,
      
    );

  $the_query = new WP_Query( $query_args ); ?>

  <?php if ( $the_query->have_posts() ) : ?>

		<div class="col-lg-12 feature-list">
			<ul class="store-list">
			<!-- the loop -->
			    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			    

			    <?php //$team_image = wp_get_attachment_url( get_post_thumbnail_id('149') ); ?>


			      <li> <?php  $feat_image = wp_get_attachment_url( get_post_thumbnail_id($the_query->ID) ); ?> 

                         <?php if(empty($feat_image ))
 							 {
 							?>	
 							<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2015/08/no-image12-370x280.jpg">
 							 <?php
 								  }
 								 else
 								{
 							?>
 							<img src="<?php echo $feat_image;?>">
 							 <?php }?>




					
					 <div class="store-details">
					    <h2><a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a></h2>
						<?php  $content = get_the_content(); 
                        $trimmed_content = wp_trim_words( $content, 30, '<a href="'. get_permalink($the_query->ID) .'"> ...Read More</a>' );
					    echo $trimmed_content; 

						?>
						</div>
				  </li>
			    <?php endwhile; ?>
			</ul>
		<ul class="page-listing">
		<?php 
		$big = 999999999; // need an unlikely integer

		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $the_query->max_num_pages
		) ); ?>
		</ul>
			

		</div>



    <!-- end of the loop -->

    <!-- pagination here -->

  <?php else:  ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>
  				
  		</div>
	</div>
</div>


<?php get_footer(); ?>


<?php } else { ?>

<?php
/**
 * Template Name: Retailer search
 *
 * 
 */

get_header(); ?>


<div class="container content"> 
  <div class="main-content ">
     <div class="row content-text ">
	   	<div class="col-lg-4 search-col search-section a-search-section ">
		   <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
	      	<div class="input-group">
					<input type="text" class="form-control" name="s" placeholder="Search Retailers"/>
					<input type="hidden" name="post_type" value="retailer" /> <!-- // hidden 'products' value -->
					<span class="input-group-btn">
						<button class="btn btn-default" alt="Search" class="btn btn-default"  type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</span>	     
	     	</div>
	     	</form><!-- /input-group -->
  		</div>
	 </div> <!--ow content-text search-section end here-->


	<div class="row content-text">
	 <div class="col-lg-12 feature-list">


<?php
    if ( isset( $_REQUEST[ 'search' ] ) ) {
        // run search query
        query_posts( array(
            's' => $_REQUEST[ 'search' ],
            'post_type' => $_REQUEST[ 'retailer' ],
            'paged' => $paged
        ) );

        // loop
        if ( have_posts() ) : while ( have_posts() ) :
            // loop through results here
        endwhile; endif;

        // return to original query
        wp_reset_query();
    }
?>
<?php 
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		$query_args = array(
								's' => $_GET['s'],
								'post_type' => 'retailer',
								'posts_per_page' => 9,
								'paged' => $paged,

							);

	    $the_query = new WP_Query( $query_args ); ?>

  <?php if ( $the_query->have_posts() ) : ?>
  	
	    <ul class="store-list a-store-list">
	    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	     <li><?php  $feat_image = wp_get_attachment_url( get_post_thumbnail_id($the_query->ID) ); ?> 

           <?php if(empty($feat_image ))
 							 {
 							?>	
 							<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2015/08/no-image12-370x280.jpg">
 							 <?php
 								  }
 								 else
 								{
 							?>
 							<img src="<?php echo $feat_image;?>">
 							 <?php }?>







	      
		 <div class="store-details">
		    <h2><a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a></h2>
			<?php  $content = get_the_content(); 
                        $trimmed_content = wp_trim_words( $content, 30, '<a href="'. get_permalink($the_query->ID) .'"> ...Read More</a>' );
					   
					   ?>
					   <p><?php  echo $trimmed_content; ?> </p>
					   
		 </div>
		 </li>
		 <?php endwhile; ?>
		 
  </ul>
		
		<ul class="page-listing">

		<?php
          $big = 999999999; // need an unlikely integer

		  echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $the_query->max_num_pages
		                    ) ); ?>
		</ul>
	 </div> 
	</div>
    
  
<?php else:  ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>
    </div>
   </div>
 
<?php 

} //main if end;

?>	
<?php get_footer(); ?>