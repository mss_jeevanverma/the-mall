<?php  

 // $language  = get_query_var( 'en');
     $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>
<?php 
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<div class="container content">
  <div class="row main-content ">
    <!----- slider-------->
<div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
      <div role="listbox" class="carousel-inner">
    <?php
    global $wpdb;

    $selectall = $wpdb->get_results("select * from wp_huge_itslider_images where slider_id='3'");
  // print_r($selectall);
    $slideimg = true;
    foreach ($selectall as $key => $value) {
             $sliderimg = $value->image_url;
             $sliderdis = $value->description;
             $slidertitle = $value->name;

?>
             <div class="item <?php if($slideimg){echo 'active';} ?>   "> 
             <img alt="900x500" data-src="holder.js/900x500/auto/#666:#666" src="<?php echo $sliderimg; ?>" data-holder-rendered="true">
          <div class="carousel-caption">
            <h3><?php echo $slidertitle; ?></h3>
            <p> - <?php echo $sliderdis; ?> - </p>
          </div>
        </div>
  

  <?php 
   $slideimg = false;
 }

    
    ?>
    
        
        
      </div>
      <ol class="carousel-indicators">
<?php
        $slideimg1 = true;
    foreach ($selectall as $key => $value) {
             $sliderimg = $value->image_url;
?>
        <li class="<?php if($slideimg1){echo 'active';} ?> " data-slide-to="<?php echo $key; ?>" data-target="#carousel-example-captions"></li>
        <?php
       
        $slideimg1 = false;
         }
        ?>
       
      </ol>
    </div>
          <!--div class="main-content">
            <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
              <div role="listbox" class="carousel-inner">
                 <?php //echo do_shortcode("[huge_it_slider id='3']"); ?>
              </div>
            </div>
         </div--><!--main-content-->
                                                        <!----- slider-------->
                                                        <!--main page-->
        <div class="main-content"><!--main page-->
          <div class="row content-text">
            <div class="col-lg-8 left-side">
              <ul class="posts">
                                                                        <!-- latest news-->
                <?php 
                    $args = array(
                    'post_type' =>'news',
                    'posts_per_page' => 1,
                    'orderby'=>'post_date',
                    'order' => 'DESC',
                    );

                    $image_posts = get_posts($args);
                    //echo "<pre>"; print_r($image_posts);
                    foreach ( $image_posts  as $post )
                    {
                    $news_post =  $post->ID;
                    }

                    $last_news = get_post($news_post);           
                    $title = $last_news->post_title;
                    $content = $last_news->post_content; 
                    $link =  $last_news->guid;
                    $news_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID), array( 500, 500) );

                    $news_text = get_post_meta($post->ID, 'image_text', true);

                    $stringCut = substr($content, 0, 180);    
                   $trim_content = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href='". get_permalink($news_post) ."' title='view details'>Read More</a>";

                    //$trim_content = wp_trim_words( $content, 22, '
                    //<a href="'. get_permalink($news_post) .'"> ...Read More</a>' );
                ?>
                <li>
                  <div class="post-img">
                    <img src="<?php echo $news_image; ?>">     
                    <?php if(strlen( $news_text)>1) { ?>
                      <span>
                        <?php echo $news_text; ?>
                      </span>
                    <?php } ?>
                </div>
                <div class="post-text">
                    <h3><a href="<?php echo $link; ?>"><?php echo $title; ?></a><span> - LOREM IPSUM - </span></h3> <p><?php echo $trim_content; ?></p>
                </div>
               </li>                                                         <!--latest news end here-->
<!-- latest event-->
                  <?php 
                  $args_event = array(
                  'post_type' =>'events',
                  'posts_per_page' => 1,
                  'orderby'=>'post_date',
                  'order' => 'DESC',
                  );

                  $event_posts = get_posts($args_event);
                  foreach ( $event_posts  as $event_post )
                  {
                  $events_post =  $event_post->ID;
                  }

                  $last_event = get_post($events_post);           
                  $event_title = $last_event->post_title;
                  $event_content = $last_event->post_content; 
                  $event_link =  $last_event->guid;
                  $event_image = wp_get_attachment_url( get_post_thumbnail_id($event_post->ID) );
                  $event_text = get_post_meta($event_post->ID, 'image_text', true);
                  //$event_trim_content = wp_trim_words( $event_content, 22, '
                  //<a href="'. get_permalink($events_post) .'"> ...Read More</a>' );
                  $stringCut = substr($event_content, 0, 180);    
                  $event_trim_content = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href='". get_permalink($events_post) ."' title='view details'>Read More</a>";
                  ?>
               <li>
                <div class="post-img">
                  <img src="<?php echo $event_image; ?>">
                   <?php if(strlen( $event_text)>1){ ?> 
                    <span> <?php echo $event_text; ?></span>
                   <?php } ?>
                </div>
                <div class="post-text">
                  <h3><a href="<?php echo $event_link; ?>"> <?php echo $event_title; ?></a> 
                      <span> - LOREM IPSUM - </span>
                  </h3>
                   <?php echo $event_trim_content; ?>
                </div>
               </li>
<!-- latest event end here-->
               <?php 
                $catPost = get_posts(get_cat_ID("24")); 
                //$catPost = query_posts('category_name=home_page_post_cat');//change this
               // $catPost =   new WP_Query( 'cat=24' );
                  //  echo"<pre>"; print_r($catPost);
                foreach ($catPost as $index => $post) : setup_postdata($post);
                  //echo "<pre>"; print_r($post); echo "</pre>"; 
                $img_text = get_post_meta($post->ID, 'image_text', true); 
                 if($index>3) {  break; }
                $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
//echo "$feat_image";
                $C_CONTENT = $post->post_content;

               ?>
                <li>
                  <div class="post-img">
                    <img src=" <?php echo $feat_image; ?>"> 
                    <?php 
                        if(strlen( $img_text)>1){ ?>
                        <span> <?php echo  $img_text; ?>  </span>
                      <?php } ?>
                  </div>
                  <div class="post-text">
                    <h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                      <span> - LITTLE THINKERS - </span>
                    </h3>
                      <?php /*$trim_content = wp_trim_words( $C_CONTENT, 22, '
                        <a href="'. get_permalink($post->ID) .'"> ...Read More</a>' ); */?>

                     <?php   
                  $C_stringCut = substr($C_CONTENT, 0, 180);    
                  $C_trim_content = substr($C_stringCut, 0, strrpos($C_stringCut, ' '))."... <a href='". get_permalink($post->ID) ."' title='view details'>Read More</a>";
                  ?>
                      <?php echo $C_trim_content; ?>
                  </div>
                </li>
                <?php  endforeach; ?>
              </ul>
            </div> <!--col-lg-8 left-side -->
           
            <?php get_sidebar(); ?>
         </div> <!--row content-text -->
        </div> <!-- end main page-->

           <?php get_footer(); ?>

      <?php } else { ?>

 <!-- .container -->


 <?php 
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
  <div class="container content"> 
  <div class="row main-content ">
    <!----- slider-------->
    <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
      <div role="listbox" class="carousel-inner">
    <?php
    global $wpdb;

    $selectall = $wpdb->get_results("select * from wp_huge_itslider_images where slider_id='3'");
  // print_r($selectall);
    $slideimg = true;
    foreach ($selectall as $key => $value) {
             $sliderimg = $value->image_url;
             $sliderdis = $value->description;
             $slidertitle = $value->name;

?>
             <div class="item <?php if($slideimg){echo 'active';} ?>   "> 
             <img alt="900x500" data-src="holder.js/900x500/auto/#666:#666" src="<?php echo $sliderimg; ?>" data-holder-rendered="true">
          <div class="carousel-caption">
            <h3><?php echo $slidertitle; ?></h3>
            <p> - <?php echo $sliderdis; ?> - </p>
          </div>
        </div>
  

  <?php 
   $slideimg = false;
 }

    
    ?>
    
        
        
      </div>
      <ol class="carousel-indicators">
<?php
        $slideimg1 = true;
    foreach ($selectall as $key => $value) {
             $sliderimg = $value->image_url;
?>
        <li class="<?php if($slideimg1){echo 'active';} ?> " data-slide-to="<?php echo $key; ?>" data-target="#carousel-example-captions"></li>
        <?php
       
        $slideimg1 = false;
         }
        ?>
       
      </ol>
    </div>
    

    
    <!----- slider-------->
    
  
    <div class="row content-text">
      <div class="col-lg-8 col-sm-8 left-side a-left-side">
        <ul class="posts a-posts">
          <?php 
                    $args = array(
                    'post_type' =>'news',
                    'posts_per_page' => 1,
                    'orderby'=>'post_date',
                    'order' => 'DESC',
                    );

                    $image_posts = get_posts($args);
                    //echo "<pre>"; print_r($image_posts);
                    foreach ( $image_posts  as $post )
                    {
                    $news_post =  $post->ID;
                    }

                    $last_news = get_post($news_post);           
                    $title = $last_news->post_title;
                    $content = $last_news->post_content; 
                    $link =  $last_news->guid;
                    $news_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID), array( 500, 500) );

                    $news_text = get_post_meta($post->ID, 'image_text', true);
                    $trim_content = wp_trim_words( $content, 22, '
                    <a href="'. get_permalink($news_post) .'"> ...Read More</a>' );
                ?>


          <li>
            <div class="post-img">
              <img src="<?php echo $news_image; ?>"> 
              <?php if(strlen( $news_text)>1) { ?>
            <span class="unique"><?php echo $news_text; ?>L</span>
            <?php } ?>
            </div>
            <div class="post-text">
              <h3><a href="<?php echo $link; ?>"><?php echo $title; ?></a> <span> - LOREM IPSUM - </span></h3>
              </h3> <?php echo $trim_content; ?>
            </div>
          </li>


         <?php 
         
                  $args_event = array(
                  'post_type' =>'events',
                  'posts_per_page' => 1,
                  'orderby'=>'post_date',
                  'order' => 'DESC',
                  );

                  $event_posts = get_posts($args_event);
                  //echo "<pre>"; print_r($event_posts);
                  foreach ( $event_posts  as $event_post )
                  {
                  $events_post =  $event_post->ID;
                  }

                  $last_event = get_post($events_post);           
                  $event_title = $last_event->post_title;
                  $event_content = $last_event->post_content; 
                  $event_link =  $last_event->guid;
                  $event_image = wp_get_attachment_url( get_post_thumbnail_id($event_post->ID) );
                  $event_text = get_post_meta($event_post->ID, 'image_text', true);
                   $event_trim_content = wp_trim_words( $event_content, 22, '
                  <a href="'. get_permalink($events_post) .'"> ...Read More</a>' );
                  ?>



         <li>
            <div class="post-img">
            <img src="<?php echo $event_image; ?>">
            <?php if(strlen( $event_text)>1){ ?> 
                    <span> <?php echo $event_text; ?></span>
                   <?php } ?>
            </div>
            <div class="post-text">
              <h3><a href="<?php echo $event_link; ?>"><?php echo $event_title; ?></a>
               <span> - LITTLE THINKERS - </span></h3>
             <?php echo $event_trim_content; ?>
            </div>
          </li>
         
         <?php 
                $catPost = get_posts(get_cat_ID("79")); 
                //$catPost = query_posts('category_name=home-cat-ar');//change this

                  //echo"<pre>"; print_r($catPost);
                foreach ($catPost as $index => $post) : setup_postdata($post);
                  //echo "<pre>"; print_r($post); echo "</pre>"; 
                $img_text = get_post_meta($post->ID, 'image_text', true); 
                  if($index>3) {  break; }
                $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
//echo "$feat_image";
                $C_CONTENT = $post->post_content;

               ?>


                <li>
                  <div class="post-img">
                    <img src=" <?php echo $feat_image; ?>"> 
                    <?php 
                        if(strlen( $img_text)>1){ ?>
                        <span class="unique"> <?php echo  $img_text; ?>  </span>
                      <?php } ?>
                  </div>
                  <div class="post-text">
                    <h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                      <span> - LITTLE THINKERS - </span>
                    </h3>
                      <?php $trim_content = wp_trim_words( $C_CONTENT, 22, '
                        <a href="'. get_permalink($post->ID) .'"> ...Read More</a>' ); ?>
                      <?php echo $trim_content; ?>
                  </div>
                </li>
                <?php  endforeach; ?>
             






         
        </ul>
      </div>
      <?php get_sidebar(); ?>
      <!-------------right side section-------------->
      <div class="col-lg-4 col-sm-4 right-side a-right-side">
      <?php //get_sidebar(); ?>
        
        </div>
       
      </div>
      <!-------------right side section close--------------> 
      
    </div>
  </div>
  </div>


      <?php } ?>
        <?php get_footer(); ?>
      <!-------------footer-------------->  