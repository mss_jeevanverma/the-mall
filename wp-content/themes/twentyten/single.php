<?php  

 // $language  = get_query_var( 'en');
 $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>


<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

 <div  class="container content"> 
  <div class="row main-content inside-content">
   <div class="content-text">
    <div class="col-lg-8 col-sm-8 left-side">
       <div class="news-left-section">


          <?php
          		$post_id = $_GET["p"];
          		$news_data = get_post($post_id); 
          		$news_date = $news_data->post_date;
          		$news_content = $news_data->post_content;
          	   $newDate = date("d-m-Y", strtotime($news_date));
          		
          		//echo "<pre>";
          		//print_r($news_data);
				//$title = $news_data->post_title;
           ?> 	
            <h1><?php echo get_the_title(); ?>

        <?php
             if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} 
        ?>

            <span><?php echo $newDate;  ?></span></h1>
		<?php 
			$content_post = get_post($post_id);
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>	
            <p><?php //echo $news_content;?> </p>
            
          </div>
      </div>


	

<?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>



<?php } else {?> 


<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div  class="container content"> 
  <div class="row main-content inside-content"> 

      <div class="col-lg-8 col-sm-8 left-side a-left-side">
          <div class="news-left-section">
            <?php
              $post_id = $_GET["p"];
              $news_data = get_post($post_id); 
              $news_date = $news_data->post_date;
              $news_content = $news_data->post_content;
               $newDate = date("d-m-Y", strtotime($news_date));
              
            
           ?>   
            <h1><?php echo get_the_title(); ?>
      <?php
       if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>

            <span><?php echo $newDate;  ?></span></h1>
		<?php 
			$content_post = get_post($post_id);
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>
            <p><?php //echo $news_content;?> </p></p>
          </div>
      </div>
      
      <!-------------right side section-------------->
      <?php get_sidebar(); ?>
      <?php get_footer(); ?>
     
      <!-------------right side section close--------------> 
      

  </div>
  </div>











 <?php } ?>
