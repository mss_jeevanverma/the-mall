
<?php  
 $language  = get_bloginfo('language');   
 if($language == "en-US"){

?>


<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php get_sidebar( 'footer' );?>

<?php wp_footer(); ?>
<!-- /#wrapper --> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script> 


        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simple-sidebar.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#main-sidebar').simpleSidebar({
                    opener: '#toggle-sidebar',
                    wrapper: '#main',
           animation: {
                       easing: "easeOutQuint"
                    },
                    sidebar: {
                        align: 'right',
             closingLinks: '.close-sb',
                    },
                    sbWrapper: {
                        display: true
                    }
                });
        $("#main-sidebar").css("opacity", "1");
            });
        </script>
<?php } else { ?>

<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
</div>
<?php get_sidebar( 'footer' );?>

<!-- /#wrapper --> 
<?php wp_footer(); ?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simple-sidebar.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#main-sidebar').simpleSidebar({
                    opener: '#toggle-sidebar',
                    wrapper: '#main',
           animation: {
                       easing: "easeOutQuint"
                    },
                    sidebar: {
                        align: 'left',
             closingLinks: '.close-sb',
                    },
                    sbWrapper: {
                        display: true
                    }
                });
        $("#main-sidebar").css("opacity", "1");
            });
        </script>



<?php } ?>

<script>
  jQuery(document).ready(function(){

    //jQuery(".widget_sp_image img").css({"height":"100%"});

  });
</script>


<script>
$(document).ready(function() {
    function toggleAccordion(li) {
        if(li.hasClass('active')) {
            li.removeClass('active');
            $('.sub-menu', li).slideUp();
            $('i', li).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-left');
        }
        else {
            $('li.active .sub-menu').slideUp();
            $('li i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-left');
            $('li.active').removeClass('active');
            li.addClass('active');
            $('.sub-menu', li).slideDown();
            $('i', li).removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-down');
        }
    };
    $('.sidebar ul li').click(function(ev) {
        ev.stopPropagation();
        toggleAccordion($(this));
	});
    $('.sidebar ul li a').click(function(ev) {
        ev.stopPropagation();
        toggleAccordion($(this).parent());
    });
	/*$('.sidebar ul li a').click(function(ev) {
        $('.sidebar .sub-menu').not($(this).parents('.sub-menu')).slideUp();
	    $(this).next('.sub-menu').slideToggle();
	    ev.stopPropagation();
	    $(this).next('i').remove();
	    $(this).append('<i class="sidebar-icon glyphicon glyphicon-chevron-down"></i>');
	});*/
});

</script>






































