<?php  

 // $language  = get_query_var( 'en');
 $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>




<?php
/**
 * The Footer widget areas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php
  /*
   * The footer widget area is triggered if any of the areas
   * have widgets. So let's check that first.
   *
   * If none of the sidebars have widgets, then let's bail early.
   */
  if (   ! is_active_sidebar( 'first-footer-widget-area'  )
    && ! is_active_sidebar( 'second-footer-widget-area' )
    && ! is_active_sidebar( 'third-footer-widget-area'  )
    && ! is_active_sidebar( 'fourth-footer-widget-area' )
  )
    return;
  // If we get this far, we have widgets. Let do this.
?>
<!--style> 

.footer {
        list-style: none !important;
}
</style-->


<!--new footer-->
<footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-sm-3 links">
          <h4>SITE MAP</h4>
          <ul class="site-map">
            <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
          </ul>
        </div>
        <div class="col-lg-2 col-sm-2 find">
          <h4>FIND US:</h4>
          <ul class="site-map">
            <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
          </ul>
        </div>
        <div class="col-lg-3 col-sm-3 social-connect">
          <h4>CONNECT WITH US:</h4>
          <ul class="social-site">
            <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
          </ul>
        </div>
        <div class="col-lg-4 col-sm-4 newletter">
        <h4>SIGN UP TO OUR NEWSLETTER</h4>
          <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?> 
                      
        </div>
      </div>
      <div class="row f-logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/f-logo.png"></a></div>
      <div class="row">
        <ul class="f-nav">
        <?php //wp_nav_menu( array( 'theme_location' => 'bottom', 'container_class' => 'f-nav' ) );
          ?>

<?php
       
      $defaults = array(
        'theme_location'  => '',
        'menu'            => 'bottom',
        'container'       => 'div',
        'container_class' => 'row',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="f-nav">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
      );

      wp_nav_menu( $defaults );

?>
          <!--li><a>Â© 2015  The Mall</a></li>
          <li><a href="#">Terms & Conditions</a></li>
          <li><a href="#">Privacy Policy</a></li>
          <li><a> Design & Development: Primal by AAS</a></li-->
        </ul>
      </div>
    </div>
  </footer>
</div>

<!--new footer End-->
<?php }else{ ?>


<?php
  /*
   * The footer widget area is triggered if any of the areas
   * have widgets. So let's check that first.
   *
   * If none of the sidebars have widgets, then let's bail early.
   */
  if (   ! is_active_sidebar( 'first-footer-widget-area'  )
    && ! is_active_sidebar( 'second-footer-widget-area' )
    && ! is_active_sidebar( 'third-footer-widget-area'  )
    && ! is_active_sidebar( 'fourth-footer-widget-area' )
  )
    return;
  // If we get this far, we have widgets. Let do this.
?>





<footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-sm-3 links a-links">
          <h4>SITE MAP</h4>
          <ul class="site-map">

            <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
          </ul>
        </div>
        <div class="col-lg-2 col-sm-2 find a-find">
          <h4>FIND US:</h4>
          <ul class="site-map">
            <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
          </ul>
        </div>
        <div class="col-lg-3 col-sm-3 social-connect a-social-connect">
          <h4>CONNECT WITH US:</h4>
          <ul class="social-site">
            <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
              
          </ul>
        </div>
        <div class="col-lg-4 col-sm-4 newletter a-newletter" style="list-style-type: none;" >
          <h4>SIGN UP TO OUR NEWSLETTER</h4>
          <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?> 
           </div>
      </div>
      <div class="row f-logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/f-logo.png"></div></a>
      <div class="row">
        <ul class="f-nav">


        <?php
       
      $defaults = array(
        'theme_location'  => '',
        'menu'            => 'bottom-ar',
        'container'       => 'div',
        'container_class' => 'row',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="f-nav">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
      );

      wp_nav_menu( $defaults );

?>
          <!--li>Â© 2015  The Mall</li>
          <li><a href="#">Terms & Conditions</a></li>
          <li><a href="#">Privacy Policy</a></li>
          <li> Design & Development: Primal by AAS</li-->
        </ul>
      </div>
    </div>
  </footer>
</div>





<?php } ?>