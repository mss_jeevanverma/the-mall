<?php  

     $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>

<?php
/**
 * Template for displaying retailer single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

 <div   class="container content"> 
  <div class="row main-content ">
    
    <div class="main-reatil-slider"><!--main-retail-slider-start-here-->


<?php 

        $retailer_post =  $_GET['p'];
        $retailer = get_post($retailer_post);
      //echo "<pre>"; print_r($retailer);   
        $retailer_title = $retailer->post_title;
        $retailer_id= $retailer->ID;
        $team_image = wp_get_attachment_url( get_post_thumbnail_id($retailer_post) );
        $retailer_content= $retailer->post_content;
          global $wpdb; 
        $fg_i = $wpdb->get_results("SELECT *  FROM `wp_postmeta` WHERE `post_id` = $retailer_id AND `meta_key` LIKE 'fg_perm_metadata' ");
       
        $sl_id = $fg_i[0]->meta_value;
if($sl_id!=0){       
?>

      <div class="inner-retail">

  
    <div class="col-md-8  col-xs-8 retail-slider ">
    <!----- slider-------->

    <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
      <div role="listbox" class="carousel-inner">
   <?php 
        $fg_img = $wpdb->get_results("select guid from wp_posts where ID in ($sl_id)");
        foreach ($fg_img as $key => $img_name)
           {
           $bannerimg = $img_name->guid;               
           ?>
        <div class="item <?php if($key==0){ echo 'active'; } ?>"> <img alt="900x500" data-src="holder.js/900x500/auto/#666:#666" src="<?php echo $bannerimg; ?>" data-holder-rendered="true">
        </div>
       <?php
		  }
		?>
      </div>
      <ol class="carousel-indicators">
        <?php 
      		foreach ($fg_img as $index => $imgs)
      		{
        ?>
        <li data-slide-to="<?php echo $index; ?>" data-target="#carousel-example-captions" class="<?php if($index==0){ echo 'active'; } ?>" ></li>
        <?php 
        }   
       ?>
      </ol>
    </div>
    
    <!----- slider-------->
      </div>

      <div class="col-md-4 col-xs-4 store-name ">
        <div class="inner-store-name">
          <span class="arrow-down"></span>
          <h2><?php echo $retailer_title;?></h2>
        </div>
      </div>
    </div>

    </div><!--main-retail-slider-close-here-->
    <?php } ?>
    <div class="clearfix"></div>
    <div class="row content-text news-content">
      <div class="col-lg-12 left-side">
          <div class="news-left-section">

            <h1><?php echo $retailer_title;?></h1>
		<?php 
			$content_post = get_post($retailer_id);
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>	
            <p><?php //echo $retailer_content ;?></p>
           
          </div>
      </div>
      
    </div>
  </div>
  </div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script> 

<!--sidebar toggle navigation--> 
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simple-sidebar.min.js"></script>
       
</div>
<?php get_footer(); ?>

      <?php } else { ?>
<!--arabic start here-->
<?php
/**
 * Template for displaying retailer single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>



  <div class="container content"> 
   <div class="row main-content">    
    <div class="main-reatil-slider"><!--main-retail-slider-start-here-->
    <?php 
                $retailer_post =  $_GET['p'];
                $retailer = get_post($retailer_post);
              //echo "<pre>"; print_r($retailer);   
                $retailer_title = $retailer->post_title;
                $retailer_id= $retailer->ID;
                $team_image = wp_get_attachment_url( get_post_thumbnail_id($retailer_post) );
                $retailer_content= $retailer->post_content;
                  global $wpdb; 
                $fg_i = $wpdb->get_results("SELECT *  FROM `wp_postmeta` WHERE `post_id` = $retailer_id AND `meta_key` LIKE 'fg_perm_metadata' ");
            
                $sl_id = $fg_i[0]->meta_value;
                if($sl_id!=0){  
              ?>
     <div class="inner-retail">

              
       <div class="col-md-8 col-xs-8 retail-slider a-retail-slider">     

    <!----- slider-------->


        <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
          <div role="listbox" class="carousel-inner">
              <?php 
                    $fg_img = $wpdb->get_results("select guid from wp_posts where ID in ($sl_id)");
                     foreach ($fg_img as $key => $img_name)
                       {
                         $bannerimg = $img_name->guid;               
                         ?>                
                    <div class="item <?php if($key==0){ echo 'active'; } ?>"> <img alt="900x500" data-src="holder.js/900x500/auto/#666:#666" src="<?php echo $bannerimg; ?>" data-holder-rendered="true">
                    </div>
                 <?php
                 }
               ?>

          </div>
        <ol class="carousel-indicators">
          <?php 
            foreach ($fg_img as $index => $imgs)  { ?>
             <li data-slide-to="<?php echo $index; ?>" data-target="#carousel-example-captions" class="<?php if($index==0){ echo 'active'; } ?>" >
            </li>
          <?php  }  ?>
        </ol>
    </div>
    <!----- slider-------->
      </div>
      <div class="col-md-4 col-xs-4 store-name a-store-name">
        <div class="inner-store-name">
          <span class="arrow-down a-arrow-down"></span>
          <h2><?php echo $retailer_title;?></h2>
        </div>
      </div>
    </div>
    </div><!--main-retail-slider-close-here-->
    <?php } ?>

	<div class="clearfix"></div>
    <div class="row content-text news-content">
      <div class="col-lg-12 left-side">
          <div class="news-left-section">
            <h1><?php echo $retailer_title;?></h1>
            <p><?php echo $retailer_content ;?></p>
          </div>
      </div>
    </div>
  </div>
  </div>
  <?php get_footer(); ?>
<?php } ?>
