<?php  

 // $language  = get_query_var( 'en');
     $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>
<?php
/**
 * Template Name: Our team
 *
 * 
 */

get_header(); ?>
<div class="container content">

       <?php 
			      $args_team = array(
			      'post_type' =>'team',
			      'orderby' => 'date', 
			      'order' => 'ASC'		   
				   );

		   			$team_posts = get_posts($args_team);
		  // echo "<pre>"; print_r($news_posts);
				   foreach ( $team_posts  as $index => $tpost )
				   {
				      $team_post =  $tpost->ID;
				      $team = get_post($team_post);
		          $team_title = $team->post_title;
		          $team_image = wp_get_attachment_url( get_post_thumbnail_id($team_post) );
		          $team_content= $team->post_content;
		          $index = $index+1;
		          if($index%2!=0){
		              
		          ?>
              <div class="row main-content out-team">
					         <div class="col-lg-6 pull-left img-team">
                      <img src="<?php echo $team_image; ?>" class="img-responsive" />
					         </div>
					         <div class="col-lg-6 pull-right right-text-side">
					            <div class=" team-member-info chairman-info">
					               <h1><?php echo $team_title; ?> </h1>
					               <p><?php echo $team_content; ?></p>  
					            </div>
					         </div>
		</div>
		           <?php }else{ ?>
						        <div class="row main-content out-team">
					         <div class="col-lg-6 pull-left img-team">
					            <div class=" team-member-info">
					               <h1><?php echo $team_title; ?> </h1>
					               <?php echo $team_content; ?>  
					            </div>
					         </div>
					         <div class="col-lg-6 right-text-side pull-right">
					         	<img src="<?php echo $team_image; ?>" class="img-responsive" />
					         </div>
					        </div>

					<?php
					          }
				      }
		           ?>




   </div>



<?php get_footer(); ?>



<!--Eng part end here-->

<?php } else { ?>
<?php
/**
 * Template Name: Our team
 *
 * 
 */

get_header(); ?>




 <div class="container content"> 



            <?php 
                $args_team = array(
                'post_type' =>'team',
                'orderby' => 'date', 
                'order' => 'ASC'
                );

            $team_posts = get_posts($args_team);
        // echo "<pre>"; print_r($news_posts);
            foreach ( $team_posts  as $index => $tpost )
               {
                  $team_post =  $tpost->ID;
                  $team = get_post($team_post);
                  $team_title = $team->post_title;
                  $team_image = wp_get_attachment_url( get_post_thumbnail_id($team_post) );
                  $team_content= $team->post_content;
                  $index = $index+1;
                  if($index%2!=0){    
            ?>  
            <div class="row main-content out-team">
                   <div class="col-lg-6 img-team pull-right"><img src="<?php echo $team_image; ?>" class="img-responsive"></div>

                 <div class="col-lg-6 pull-left img-team">
                <div class=" team-member-info chairman-info"><h1><?php echo $team_title; ?> </h1>
                   <?php echo $team_content; ?>  

                </div>
                </div>
               
            </div>

            <?php }else{ ?>  
			  <div class="row main-content out-team">
            <div class="col-lg-6 right-text-side pull-left"><img src="<?php echo $team_image; ?>" class="img-responsive" >
                  </div>
                <div class="col-lg-6 pull-right img-team">
                   <div class=" team-member-info">
                    <h1><?php echo $team_title; ?>  </h1>
                    <?php echo $team_content; ?>
                   </div>
               </div>
          
             </div>
		   
          <?php
           }
         }  // end of foreach
       ?>
   
   </div>
   

<?php get_footer(); ?>

<?php } ?>