<?php  

 // $language  = get_query_var( 'en');
 $language  = get_bloginfo('language');   
  //print_r($language);
  if($language == "en-US"){

?>


<?php
/**
 * Template Name: Latest News
 *
 * 
 */

get_header(); ?>

<div   class="container content"> 
  <div class="row main-content ">
      <div class="col-lg-12 col-sm-12 latest-news-post">
		<ul class="posts">
		      <?php 
			         $args1 = array(
			        'post_type' =>'news',
			       
				    'orderby'=>'post_date',
				    'posts_per_page' => 6,
				   
				     );

		   			$news_posts = get_posts($args1);
		  // echo "<pre>"; print_r($news_posts);
				   foreach ( $news_posts  as $npost )
				   {
				      $news_post =  $npost->ID;
				      $all_news = get_post($news_post);
				      //echo "<pre>"; print_r($all_news);
		              $title = $all_news->post_title;
		              $new_image = wp_get_attachment_url( get_post_thumbnail_id($npost->ID) );
		              $news_text1 = get_post_meta($npost->ID, 'image_text', true);
		              $all_content = $all_news->post_content; 
		              $all_news_link =  $all_news->guid;
		              $news_publish_date1 = $all_news->post_date;
		              $news_publish_date = date("d-m-Y", strtotime($news_publish_date1));
		              $trimmed_content = wp_trim_words( $all_content, 14, '<a href="'. get_permalink($news_post) .'"> ...Read More</a>' );

		              ?>
         		 <li>
           			 <div class="post-img">

                        <?php
									            	if(empty($new_image ))
 									                 {
 									                  ?>	
 									                   <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2015/08/no-image12-370x280.jpg">
 									                 <?php
 									                 }
 									               else
 									                {
 									                 	?>
 									    	          <img src="<?php echo $new_image;?>">
 									                 <?php
 									                }?>


           		
           			 <?php/* if(strlen($news_text1)>1){?><span><?php// echo $news_text1;?></span><?php }*/?> 
           			 </div>
            			<div class="post-text">
            			  <h3><a href="<?php echo $all_news_link; ?>"><?php echo $title; ?></a> <span> - <?php echo $news_publish_date; ?> - </span></h3>
              				<p><?php echo $trimmed_content; ?> </p>
            			</div>
            
          		</li>
         



		         
		          <?php 
		            }
		            ?>


         </ul> 
      </div>
    </div>
  </div>

<?php get_footer(); ?>


<?php } else { ?>

<?php
/**
 * Template Name: Latest News
 *
 * 
 */

get_header(); ?>
<div class="container content"> 
  <div class="row main-content ">
   
    
	
      <div class="col-lg-12 col-sm-12 latest-news-post">
        <ul class="posts a-posts">
          
        	<?php 
			         $args1 = array(
			        'post_type' =>'news',
			       
				    'orderby'=>'post_date',
				    'posts_per_page' => 6,
				   
				     );

		   			$news_posts = get_posts($args1);
		  // echo "<pre>"; print_r($news_posts);
				   foreach ( $news_posts  as $npost )
				   {
				      $news_post =  $npost->ID;
				      $all_news = get_post($news_post);
				      //echo "<pre>"; print_r($all_news);
		              $title = $all_news->post_title;
		              $new_image = wp_get_attachment_url( get_post_thumbnail_id($npost->ID) );
		              $news_text1 = get_post_meta($npost->ID, 'image_text', true);
		              $all_content = $all_news->post_content; 
		              $all_news_link =  $all_news->guid;
		              $news_publish_date1 = $all_news->post_date;
		              $news_publish_date = date("d-m-Y", strtotime($news_publish_date1));
		              $trimmed_content = wp_trim_words( $all_content, 14, '<a href="'. get_permalink($news_post) .'"> ...Read More</a>' );

		              ?>
         		
          <li>
            <div class="post-img">
            <?php if(empty($new_image ))
 									                 {
 									                  ?>	
 									                   <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2015/08/no-image12-370x280.jpg">
 									                 <?php
 									                 }
 									               else
 									                {
 									                 	?>
 									    	          <img src="<?php echo $new_image;?>">
 									                 <?php
 									                }?>
             <?php/* if(strlen($news_text1)>1){?><span><?php// echo $news_text1;?></span><?php }*/?> </div>
            <div class="post-text">
              <h3><a href="<?php echo $all_news_link; ?>"><?php echo $title; ?></a> <span > - <?php echo $news_publish_date; ?>  - </span></h3>
              <p><?php echo $trimmed_content; ?> </p>
            </div>
          </li>
          <?php 
		            }
		            ?>
          
        </ul>
      </div>

      
    </div>
  </div>




<?php get_footer(); ?>
<?php } ?>