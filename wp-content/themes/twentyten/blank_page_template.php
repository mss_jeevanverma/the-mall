<?php
/**
 * Template Name: Blank Page Template
 *
 * 
 */
get_header(); ?>
<!-- .container -->
<div class="container content">   
   <div class="main-content">
    <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">      
      
<!--main page-->
     
      
       <div class="row content-text">
        
        <?php  while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
    
         <?php endwhile; ?> 

        </div>

</div>

</div>

<!--main page end here-->

		
<?php //get_sidebar(); ?>
</div>

<?php get_footer(); ?>
