<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<!-- .container -->
		<div class="container content">

   <!----- slider-------->
   <div class="main-content">

    <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
      
      <div role="listbox" class="carousel-inner">

      <?php echo do_shortcode("[huge_it_slider id='3']"); ?>
     

  </div>
  </div>
  </div>
        <!----- slider-------->


<!--main page-->
     
       <div class="main-content">
       <div class="row content-text">
        <div class="col-lg-8 left-side">
        <ul class="posts">
        <!-- latest news-->
        <?php 
	         $args = array(
	        'post_type' =>'news',
	        'posts_per_page' => 1,
		    'orderby'=>'post_date',
		    'order' => 'DESC',
		     );

		       $image_posts = get_posts($args);
		       foreach ( $image_posts  as $post )
		         {
		          $news_post =  $post->ID;
		         }

             $last_news = get_post($news_post);           
             $title = $last_news->post_title;
             $content = $last_news->post_content; 
             $link =  $last_news->guid;
             $news_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID), array( 500, 500) );
          
             $news_text = get_post_meta($post->ID, 'image_text', true);
             $trim_content = wp_trim_words( $content, 15, '<a href="'. get_permalink($news_post) .'"> ...Read More</a>' );
         ?>

          <li>
           <div class="post-img"><img src="<?php echo $news_image; ?>"> 

           <?php 
                if(strlen( $news_text)>1){
              ?>
               <span><?php echo $news_text; ?></span>
             <?php } ?> </div>
           <div class="post-text">
           <h3><a href="<?php echo $link; ?>"><?php echo $title; ?></a> <span> - LOREM IPSUM - </span></h3>
           <?php echo $trim_content; ?>
           </div>
          </li>
          <!--latest news end here-->
          <!-- latest event-->
          <?php 
           $args_event = array(
          'post_type' =>'events',
          'posts_per_page' => 1,
          'orderby'=>'post_date',
         'order' => 'DESC',
         );

           $event_posts = get_posts($args_event);
           foreach ( $event_posts  as $event_post )
             {
              $events_post =  $event_post->ID;
             }

             $last_event = get_post($events_post);           
             $event_title = $last_event->post_title;
             $event_content = $last_event->post_content; 
             $event_link =  $last_event->guid;
             $event_image = wp_get_attachment_url( get_post_thumbnail_id($event_post->ID) );
             $event_text = get_post_meta($event_post->ID, 'image_text', true);
             $event_trim_content = wp_trim_words( $content, 15, '<a href="'. get_permalink($events_post) .'"> ...Read More</a>' );
         ?>

          <li>
           <div class="post-img"><img src="<?php echo $event_image; ?>"> 
            <?php 
                if(strlen( $event_text)>1){
              ?>
               <span><?php echo $event_text; ?></span>
             <?php } ?>
            </div>
           <div class="post-text">
           <h3><a href="<?php echo $event_link; ?>"><?php echo $event_title; ?></a> <span> - LOREM IPSUM - </span></h3>
           <?php echo $event_trim_content; ?>
           </div>
          </li>






          <!-- latest event end here-->
          

   			 <?php 
 					$catPost = get_posts(get_cat_ID("home_page_post_cat")); //change this
   					foreach ($catPost as $index => $post) : setup_postdata($post);
                         //echo "<pre>"; print_r($post); echo "</pre>"; 
                         $img_text = get_post_meta($post->ID, 'image_text', true); 
                        if($index>3)
                        {
                           break;
                          }
                   $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                   //echo "$feat_image";

                       $C_CONTENT = $post->post_content;

   					 ?>
     		 
          <li>
           <div class="post-img"><img src="<?php echo $feat_image; ?>"> 
             <?php 
                if(strlen( $img_text)>1){
             	?>
               <span>	<?php echo  $img_text; ?>	</span>
             <?php } ?>
           </div>
           <div class="post-text">
           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <span> - LITTLE THINKERS - </span></h3>
           <?php $trim_content = wp_trim_words( $C_CONTENT, 15, '<a href="'. get_permalink($post->ID) .'"> ...Read More</a>' ); ?>
           <?php echo $trim_content; ?>

           </div>
          </li>
          
          <?php  endforeach; ?>
          
          
         
        </ul>
        </div>





<!--main page end here-->

		
<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
