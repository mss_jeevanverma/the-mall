<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

    </div> 
        
<!-------------footer-------------->
          <?php get_sidebar( 'footer' );?>
       <?php /* <footer class="footer">
           <div class="container">
             <div class="row">
                <div class="col-lg-3">
                  <h4>SITE MAP</h4>
                  <ul class="site-map">
                    <li><a href="#">Shopping</a></li>
                    <li><a href="#">Dining</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Info</a></li>
                  </ul>
                </div>
                <div class="col-lg-3">
                 <h4>FIND US:</h4>
                  <ul class="site-map">
                    <li><a href="#">Google Map</a></li>
                    <li><a href="#">Location Map</a></li>
                  </ul>
                </div>
                <div class="col-lg-3">
                <h4>CONNECT WITH US:</h4>
                <ul class="social-site">
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
                </div>
                <div class="col-lg-3">
                   
         
                   <!--form role="form" class="footer-form">
                       <div class="form-group">
                       
                          <!--input type="text" class="form-control" id="name" placeholder="name">
                       </div> 
                       <div class="form-group">
                         <input type="email" class="form-control" id="email" placeholder="Enter email">
                       </div>
                       <div class="buton-form">
                          <button type="submit" class="btn btn-default f-submit">SUBMIT</button>
                          </div>
                   </form-->
                   
                </div>
             
             </div>
             
             
             <div class="row f-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/f-logo.png"></div>
             
             <div class="row">
              <ul class="f-nav">
               <li>© 2015  The Mall</li>
               <li><a href="#">Terms & Conditions</a></li>
               <li><a href="#">Privacly Policy</a></li>
               <li> Design & Development: Primal by AAS</li>
              </ul>
             </div>
           </div>
        </footer>
       */ ?>
        
	</div>
	<!-- /#wrapper -->
<?php wp_footer(); ?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>
  
  
 
	
	
  
	
  </body>
</html>






