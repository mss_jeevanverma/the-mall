<?php  
 $language  = get_bloginfo('language');   
 if($language == "en-US"){

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo $site_title = get_bloginfo( 'name' ); ?></title>

<!-- Bootstrap -->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome (font icons) - http://goo.gl/XfBd3 -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/arabic-css.css" rel="stylesheet" type="text/css">
 <?php wp_head(); ?> 
<!----------sidebar toggle navigation--------->
	<script>
		$(".nav_trigger").click(function() {
			$("body").toggleClass("show_sidebar");
			$(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); 
		});
	</script>
</head>

<body>

<!-- #wrapper -->
<div id="wrapper"> 
<header  class="main-navbar main-navbar-fixed-top header">

      <div class=" container main-navbar-content">
      <div class="row">
        <div class="navbar-brand logo">
          <a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"></a>
        </div>
           
            <span id="toggle-sidebar" class="nav_trigger icon right "><i class="fa fa-navicon"></i></span>







  <?php 
               $uri = $_SERVER['REQUEST_URI'];
               $elms = explode('/', $uri) ;
               $firstcat = $elms[3] ;
              // $cat_id= get_cat_ID( $firstcat );
               $cat_id = get_query_var('cat');;
               echo $cat_name = get_cat_name( $cat_id );
          ?>
            <span id="toggle-sidebar" class="nav_trigger icon right "><!--i class="fa fa-navicon"></i--></span>
           <?php if(is_home())
              {?>
            <span class="page-heading">REFRESHINGLY DIFFERENT</span>
             <?php 
                }elseif(strlen($cat_name)>1){
               ?>
            <span class="page-heading">
              <?php echo $cat_name; ?>
            </span>
              <?php
              }else{ ?>
            <span class="page-heading">
             <?php echo get_the_title(); ?>
            </span>
          <?php } ?>
      </div>
      </div>

     </header>  
    

 <!-- /#push_sidebar --> 
<div id="main-sidebar" class="main-sidebar main-sidebar-right">
 <div class="sidebar-nav">
  <div class="navbar navbar-default nav-bg" role="navigation">
    <div class="navbar-collapse collapse sidebar-navbar-collapse nav-space">
      <ul class="nav navbar-nav" id="sidenav01">
      

<?php
$defaults = array(
  'theme_location' => '', 'menu' => 'headermenu', 'container' => 'div',
  'container_class' => 'nav navbar-nav a-navbar-nav', 'container_id' => 'sidenav01', 'menu_class' => 'nav nav-list',
  'menu_id' => 'sidenav01', 'echo' => true, 'fallback_cb' => 'wp_page_menu',
  'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '',
  'items_wrap' => '<ul id="sidenav01" class="nav navbar-nav">%3$s</ul>',
  'depth'           => 0, 'walker' => new My_Walker_Nav_Menu()
);

wp_nav_menu( $defaults );
 


?>

         <!--li><a href="#"> HOME</a></li>
     <li><a href="#"> About Us</a></li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav01" class="collapsed">
           RETAILERS-PAGE 22 <span class="caret pull-right"></span>
          </a>
          <div class="collapse" id="toggleDemo2" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="#">RETAILERS-PAGE 22</a></li>
              <li><a href="#">RETAILERS-PAGE 22</a></li>
              <li><a href="#">RETAILERS-PAGE 22</a></li-->
            <!--/ul>
          </div>
        </li>
        <li><a href="#">CONTACT US</a></li>
        <li><a href="#">SERVICES</a></li>
        <li><a href="">OUR TEAM</a></li-->
      </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>





<?php } else { ?>



<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo $site_title = get_bloginfo( 'name' ); ?></title>

<!-- Bootstrap -->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome (font icons) - http://goo.gl/XfBd3 -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/arabic-css.css" rel="stylesheet" type="text/css">
 <?php wp_head(); ?>
<!----------sidebar toggle navigation--------->
	<script>
		$(".nav_trigger").click(function() {
			$("body").toggleClass("show_sidebar");
			$(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); 
		});
	</script> 
</head>
<body class="arabic">


<!-- #wrapper -->
<div id="wrapper"> 
  <!-- header-->
  
  <header class="main-navbar main-navbar-fixed-top header">
 <div>
            <div class=" container main-navbar-content">
       <div class="navbar-brand logo a-logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"></a></div>
         <span id="toggle-sidebar" class="nav_trigger icon right a-nav_trigger "><i class="fa fa-navicon"></i></span> 
           <?php 
               $uri = $_SERVER['REQUEST_URI'];
               $elms = explode('/', $uri) ;
               $firstcat = $elms[3] ;
              // $cat_id= get_cat_ID( $firstcat );
               $cat_id = get_query_var('cat');;
               echo $cat_name = get_cat_name( $cat_id );
          ?>
            <span id="toggle-sidebar" class="nav_trigger icon right "><!--i class="fa fa-navicon"></i--></span>
           <?php if(is_home())
              {?>
            <span class="page-heading">REFRESHINGLY DIFFERENT</span>
             <?php 
                }elseif(strlen($cat_name)>1){
               ?>
            <span class="page-heading">
              <?php echo $cat_name; ?>
            </span>
              <?php
              }else{ ?>
            <span class="page-heading">
             <?php echo get_the_title(); ?>
            </span>
          <?php } ?>
         </div>
      </div>
       
  </header>
   
  <!-- header --> 
  
  <!-- /#push_sidebar --> 
<div id="main-sidebar" class="main-sidebar main-sidebar-right">
  <div class="sidebar-nav a-sidebar-nav">
   <div class="navbar navbar-default nav-bg" role="navigation">
    <div class="navbar-collapse collapse sidebar-navbar-collapse nav-space">
      <ul class="nav navbar-nav a-navbar-nav" id="sidenav01">




<?php
$defaults = array(
  'theme_location' => '', 'menu' => 'arabic_menu', 'container' => 'div',
  'container_class' => 'nav navbar-nav a-navbar-nav', 'container_id' => 'sidenav01', 'menu_class' => 'nav nav-list',
  'menu_id' => 'sidenav01', 'echo' => true, 'fallback_cb' => 'wp_page_menu',
  'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '',
  'items_wrap' => '<ul id="sidenav01" class="nav navbar-nav a-navbar-na">%3$s</ul>',
  'depth'           => 0, 'walker' => new My_Walker_Nav_Menu()
);

wp_nav_menu( $defaults );
 


?>



         <!--li><a href="#"> Home</a></li>
         <li><a href="#"> About Us</a></li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav01" class="collapsed">
           RETAILERS-PAGE 22 <span class="caret pull-right"></span>
          </a>
          <div class="collapse" id="toggleDemo2" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="#">RETAILERS-PAGE 22</a></li>
              <li><a href="#">RETAILERS-PAGE 22</a></li>
              <li><a href="#">RETAILERS-PAGE 22</a></li>
            </ul>
          </div>
        </li>
        <li><a href="#">CONTACT US</a></li>
        <li><a href="#">SERVICES</a></li>
        <li><a href="">OUR TEAM</a></li-->
      </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
 </div>

<?php } ?>